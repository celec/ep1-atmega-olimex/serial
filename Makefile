OUT ?= exec
SRC = $(wildcard *.c)
OBJ = $(SRC:.c=.o)
DEPS = $(wildcard *.h)

CC = avr-gcc
MCU = atmega32u4
ARG = -mmcu=$(MCU) -Wall -Os
REP = ./lib/
LIB = VirtualSerial
INC = lufa-LUFA-140928
INCLIB = -I$(REP)$(INC)/ 
SERIAL = -DF_USB=16000000UL -std=gnu99

all: $(OUT)
	make -s flash clean

$(OUT): $(OBJ)
	$(CC) $(ARG) $(INCLIB) -L. -o $@ $^ -l$(LIB)

%.o: %.c $(DEPS)
	$(CC) $(ARG) $(INCLIB) $(SERIAL) -o $@ -c $< -I.

clean:
	rm -f *.o $(OUT)

flash: $(OUT)
	avrdude -c avr109 -b57600 -D -p $(MCU) -P /dev/ttyACM0 -e -U flash:w:$^
